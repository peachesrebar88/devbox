#!/bin/sh

rsync -e 'ssh -4' -raxv --delete upstream_docs/i686-sel4-unknown/doc/ cmr@octayn.net:doc.robigalia.org/i686
rsync -e 'ssh -4' -raxv --delete upstream_docs/arm-sel4-gnueabi/doc/ cmr@octayn.net:doc.robigalia.org/arm
rsync -e 'ssh -4' -raxv --delete upstream_docs/index.html cmr@octayn.net:doc.robigalia.org/
